# AutoCSS

AutoCSS is a tool that generates CSS from a simple syntax that can be written as class-names in a HTML-document.
It can be used to implement complex designs while keeping your CSS line-count to a minimum.


## Basic example

```
<div class="pa:4 c:$white bc:$pink">
  HELLO
</div>
```

Produces the following CSS:
```
.pa\:4 {
  padding: 4rem;
}
.c\:white {
  color: var(--color-white);
}
.bc\:hotpink {
  background-color: var(--color-pink);
}

```

This example uses variables, so you will have to set them manually,
using CSS4:
```
:root {
  --color-white: white;
  --color-pink: hotpink;
}

```
Or SASS:
```
$color-white: white;
$color-pink: hotpink;
```


## More examples

```
d:b -> display: block
d:ib -> display: inline-block
d:b@m -> display: block @ media device-width < 420px

c:#fff -> color: #fff
bc:#000 -> background-color: #000
c:$a -> color: var(--color-a)
bc:$b -> background-color: var(--color-b)

fs:1 -> font-size: 1rem
fs:24px -> font-size: 24px
lh:1 -> line-height: 1
lh:30px -> line-height: 30px

```

See autocss.options.map for a complete list of properties.


## Usage

See .../autocss-demo.git for usage examples.


## Questions?

### How is this different from inline-styles?
* AutoCSS has a different, less verbose syntax.
* AutoCSS can be cached.

### Can I use this with SASS
Yes, setting variableTemplate to "$$name" will produce SASS-compatible output.

### Can I use this with Gulp?
Yes, there's a (very basic) gulp plugin, see gulp-autocss.

### How can I use this when I set my class-attributes dynamically?
You have a few alternatives:
* You can use the "preload" config option
* Use a static HTML-file with all your dynamic classes
* Write a custom parser

