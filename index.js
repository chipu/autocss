
var isBrowser = typeof exports == 'undefined' && typeof window != 'undefined'

// From https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Object/assign
var objectAssign = function(target, varArgs) { // .length of function is 2
  'use strict'
  if(target == null) { // TypeError if undefined or null
    throw new TypeError('Cannot convert undefined or null to object')
  }
  var to = Object(target)
  for(var index = 1; index < arguments.length; index++) {
    var nextSource = arguments[index]
    if(nextSource != null) { // Skip over if undefined or null
      for(var nextKey in nextSource) {
        // Avoid bugs when hasOwnProperty is shadowed
        if(Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
          to[nextKey] = nextSource[nextKey]
        }
      }
    }
  }
  return to
}

var objectValues = function(obj) {
  if(Object.values) {
    return Object.values(obj)
  }
  return Object.keys(obj).map(function(key) {
    return obj[key]
  })
}

var defaults = {
  cache: true,
  debug: console.log,
  warning: console.log,
  error: console.log,
  vars: {
    size: 'rem',
  },
  variableTemplate: 'var(--$name)',
  media: {
    '=d': 'screen and (min-width: 1024px)',
    '<d': 'screen and (max-width: 1023px)',
    '=t': 'screen and (min-width: 480px) and (max-width: 1023px)',
    '>t': 'screen and (min-width: 1023px)',
    '<t': 'screen and (max-width: 479px)',
    '=m': 'screen and (max-width: 479px)',
    '>m': 'screen and (min-width: 480px)',
  },
  classes: {

    // Templates
    _size: {
      val: {
        unit: [/^(\-|)(\d+|\d[\.\d]+)(em|rem|px|pt|vw|vh|%)$/, '$1$2$3'],
        unitless: [/^(\-|)(\d+|\d[\.\d]+)$/, '$1$2$size'],
      }
    },
    _unit: {
      extends: '_size',
      val: {
        'a': 'auto',
        'n': 'none'
      }
    },
    _color: {
      val: {
        hex: [/^#([0-9abcdef]+)$/, '#$1'],
        named: [/^(\w+)$/, '$1'],
      }
    },
    _duration: {
      val: {
        unit: [/^(\-|)(\d+|\d[\.\d]+)(s)$/, '$1$2$3'],
      }
    },
    _angle: {
      val: {
        unit: [/^(\-|)(\d+|\d[\.\d]+)(deg|rad|turns)$/, '$1$2$3'],
      }
    },
    _named: {
      val: {
        unit: [/^([\_\-\w]+)$/, '$1'],
      }
    },
    '_border_style': {
      'val': {
        's': 'solid',
        'd': 'dotted',
        'da': 'dashed',
      },
    },

    // Basic
    'bs': {
      'key': 'box-sizing',
      'val': {
        'bb': 'border-box',
      },
    },
    'f': {
      'key': 'float',
      'val': {
        'l': 'left',
        'r': 'right',
      }
    },
    'd': {
      'key': 'display',
      'val': {
        'fl': 'flex',
        'n': 'none',
        'b': 'block',
        'i': 'inline',
        'ib': 'inline-block',
        't': 'table',
        'tc': 'table-cell',
      }
    },

    // Flexbox
    'flw': {
      'key': 'flex-wrap',
      'val': {
        'w': 'wrap',
      }
    },
    'ai': {
      'key': 'align-items',
      'val': {
        'c': 'center',
      }
    },
    'jc': {
      'key': 'justify-content',
      'val': {
        'c': 'center',
      }
    },

    // Tables
    'va': {
      'key': 'vertical-align',
      'val': {
        'm': 'middle',
      }
    },

    // Colors
    'c': {
      'extends': '_color',
      'var': 'color',
      'key': 'color',
    },
    'bc': {
      'extends': '_color',
      'var': 'color',
      'key': 'background-color',
    },

    // Text
    'ta': {
      'key': 'text-align',
      'val': {
        'l': 'left',
        'r': 'right',
        'c': 'center',
      },
    },
    'tt': {
      'key': 'text-transform',
      'val': {
        'u': 'uppercase',
      },
    },
    'fs': {
      'var': 'font-size',
      'key': 'font-size',
      'extends': '_unit'
    },
    'lh': {
      'extends': '_size',
      'var': 'line-height',
      'key': 'line-height',
      'vals': {'$size': ''},
    },
    'ls': {
      'extends': '_size',
      'var': 'letter-spacing',
      'key': 'letter-spacing',
      'vals': {'$size': ''},
    },
    'fw': {
      'extends': '_unit',
      'var': 'font-weight',
      val: {
        n: 'normal',
        b: 'bold',
        bo: 'bolder',
        l: 'lighter',
        formal: [/^(normal|bold|bolder|lighter|100|200|300|400|500|600|700|800|900)$/, '$1'],
        global: [/^(inherit|initial|unset)$/, '$1'],
      }
    },

    // Sizes & spacings
    'w': {
      'extends': '_unit',
      'key': 'width',
    },
    'h': {
      'extends': '_unit',
      'key': 'height',
    },
    'mw': {
      'extends': '_unit',
      'var': 'width',
      'key': 'max-width',
    },
    'ma': {
      'extends': '_unit',
      'var': 'spacing',
      'key': ['margin'],
    },
    'ml': {
      'extends': '_unit',
      'var': 'spacing',
      'key': ['margin-left'],
    },
    'mr': {
      'extends': '_unit',
      'var': 'spacing',
      'key': ['margin-right'],
    },
    'mt': {
      'extends': '_unit',
      'var': 'spacing',
      'key': ['margin-top'],
    },
    'mb': {
      'extends': '_unit',
      'var': 'spacing',
      'key': ['margin-bottom'],
    },
    'mtb': {
      'extends': '_unit',
      'var': 'spacing',
      'key': ['margin-top', 'margin-bottom'],
    },
    'mlr': {
      'extends': '_unit',
      'var': 'spacing',
      'key': ['margin-left', 'margin-right'],
    },
    'pa': {
      'extends': '_unit',
      'var': 'spacing',
      'key': ['padding'],
    },
    'pl': {
      'extends': '_unit',
      'var': 'spacing',
      'key': ['padding-left'],
    },
    'pr': {
      'extends': '_unit',
      'var': 'spacing',
      'key': ['padding-right'],
    },
    'pt': {
      'extends': '_unit',
      'var': 'spacing',
      'key': ['padding-top'],
    },
    'pb': {
      'extends': '_unit',
      'var': 'spacing',
      'key': ['padding-bottom'],
    },
    'ptb': {
      'extends': '_unit',
      'var': 'spacing',
      'key': ['padding-top', 'padding-bottom'],
    },
    'plr': {
      'extends': '_unit',
      'var': 'spacing',
      'key': ['padding-left', 'padding-right'],
    },

    // Borders
    'bos': {
      'extends': '_border_style',
      'key': 'border-style',
    },
    'bts': {
      'extends': '_border_style',
      'key': 'border-top-style',
    },
    'brs': {
      'extends': '_border_style',
      'key': 'border-right-style',
    },
    'bbs': {
      'extends': '_border_style',
      'key': 'border-bottom-style',
    },
    'bls': {
      'extends': '_border_style',
      'key': 'border-left-style',
    },
    'boc': {
      'extends': '_color',
      'key': 'border-color',
      'var': 'color',
    },
    'btc': {
      'extends': '_color',
      'var': 'color',
      'key': 'border-top-color',
    },
    'brc': {
      'extends': '_color',
      'key': 'border-right-color',
      'var': 'color',
    },
    'bbc': {
      'extends': '_color',
      'key': 'border-bottom-color',
      'var': 'color',
    },
    'blc': {
      'extends': '_color',
      'key': 'border-left-color',
      'var': 'color',
    },
    'bow': {
      'extends': '_size',
      'key': 'border-width',
    },
    'btw': {
      'extends': '_size',
      'key': 'border-top-width',
    },
    'brw': {
      'extends': '_size',
      'key': 'border-right-width',
    },
    'bbw': {
      'extends': '_size',
      'key': 'border-bottom-width',
    },
    'blw': {
      'extends': '_size',
      'key': 'border-left-width',
    },
    'bor': {
      'extends': '_size',
      'key': 'border-radius',
    },
    'btr': {
      'extends': '_size',
      'key': 'border-top-radius',
    },
    'bbr': {
      'extends': '_size',
      'key': 'border-bottom-radius',
    },
    'brr': {
      'extends': '_size',
      'key': 'border-right-radius',
    },
    'blr': {
      'extends': '_size',
      'key': 'border-left-radius',
    },

    // Animation
    'a': {
      'extends': '_named',
      'key': 'animation',
    },
    't': {
      'extends': '_duration',
      'key': 'transition',
    },
  }
}

var cache = {}

var isValueMatch = function(k, inval) {
  if(k instanceof RegExp) {
    return inval.match(k)
  }
  return k == inval
}

var camelDash = function(str) {
  return str.replace(/[A-Z]/g, function(m) {
    return '-' + m.toLowerCase()
  })
}

var objToCss = function(obj) {
  var css = ''
  for(var k in obj) {
    css += camelDash(k) + ": " + obj[k] + ";\n"
  }
  return css
}

var getClassConfig = function(key, options) {
  if(!options.classes[key]) {
    throw 'Class not found: ' + key
  }
  var result = options.classes[key]
  if(result['extends']) {
    var base = getClassConfig(result['extends'], options)
    if(base['val'] && result['val']) {
      result.val = objectAssign({}, base.val, result.val)
    }
    result = objectAssign({}, base, result)
  }
  return result
}

var getSingle = function(className, options) {
  if(options.cache && cache[className]) {
    return cache[className]
  }

  if(options.classes[className]) {
    var config = getClassConfig(className, options)
    var css = ''
    if(config.include) {
      var inc = config.include instanceof Array ? config.include : config.include.split(' ')
      var tmp = autocss.css(inc, options)
      // TODO: Work with media queries and :hover
      // TODO: Internal functions that just returns the attributes & values
      css = tmp.split("\n").filter(function(item) {
        return item.indexOf(';') > -1
      }).join("\n")
    }
    if(config.css) {
      css += objToCss(config.css)
    }
    var css = wrapCss(className, css, '', '')
    if(!css) {
      throw 'Simple classes should have a css or include property'
    }
    // FIXME: Cache & media
    return {
      className: className,
      media: null,
      css: css,
    }
  }

  var match = className.match(/^([a-zA-Z0-9]+)\:([\$]*)(.+)/)
  if(!match) {
    options.debug('Debug: Class is not a match: ' + className)
    return null
  }

  var key = match[1]
  var type = match[2]
  var inval = match[3]
  var media = null
  var value = null
  var before = ''
  var after = ''

  if(match = inval.match(/^(.+)\@(.+)$/)) {
    inval = match[1]
    media = match[2]
    if(!media.match(/^[\<\>\=]/)) {
      media = '=' + media
    }
    if(!options.media[media]) {
      options.debug('Warning: Media not set: ' + className + ', ' + media)
      return null
    }
  }

  if(match = inval.match(/^(.+)([\&\~]{1})(.+)$/)) {
    inval = match[1]
    switch(match[2]) {
    case '&':
      after = match[3]
      break
    case '~':
      before = match[3] + ' '
      break
    default:
      throw "Not supported"
    }
  }

  if(!options.classes[key]) {
    options.debug('Warning: Class not configured: ' + className + ', ' + key)
    return null
  }

  var config = getClassConfig(key, options)
  var keys = config['key'] instanceof Array ? config['key'] : [config['key']]

  switch(type) {
  case '':
    if(!config['val']) {
      throw "Error: " + className + " is not a val class\n";
    }
    for(var k in config['val']) {
      var v = config['val'][k]
      if(v instanceof Array) {
        k = v[0]
        v = v[1]
      }
      var m = isValueMatch(k, inval);
      if(m) {
        var vars = objectAssign({}, options.vars, config['vals'] ? config['vals'] : {})
        if(typeof m == 'object') {
          vars = objectAssign({}, vars, m)
        }
        value = v.replace(/\$([a-zA-Z0-9]+)/g, function(_, m) {
          var res = vars[m]
          if(res instanceof Function) {
            res = res(this)
          }
          return res
        })
        break;
      }
    }
    if(value === null) {
      throw "Error: '" + className + "' value is null, inval='" + inval + "'\n";
    }
    break
  case '$':
    if(!config['var']) {
      throw "Error: " + className + " is not a var class\n";
    }
    value = 'var(--' + config['var'] + '-' + inval + ')';
    break;
  default:
    throw 'Unsupported type: ' + type
  }
  var css = wrapCss(className, keys.map(function(item) {
    return "  " + item + ": " + value + ";\n";
  }).join("\n"), before, after)
  var result = {
    className: className,
    media: media ? options.media[media] : null,
    css: css
  }
  cache[className] = result
  return result
}

var wrapCss = function(className, content, before, after) {
  var name = className.replace(/[^a-zA-Z0-9\-_]/g, function(match) {
    return '\\' + match
  })
  var css = before + "." + name + after + " {\n";
  css += content
  css += "}\n"
  return css
}

var autocss = {
  options: defaults,
  parse: function(input, options) {
    options = objectAssign({}, autocss.options, options ? options : {})
    if(input instanceof Array) {
      var items = input.map(function(item) {
        return getSingle(item, options)
      })
      var media = {}
      for(var i = 0; i < items.length; ++i) {
        var item = items[i]
        if(item && item.css.length) {
          if(!media[item.media]) {
            media[item.media] = {
              media: item.media,
              css: {}
            }
          }
          media[item.media].css[item.className] = item.css
        }
      }
      return objectValues(media).map(function(item) {
        return {
          media: item.media,
          css: objectValues(item.css)
        }
      })
    }
    return getSingle(input, options)
  },
  css: function(input, options) {
    var result = this.parse(input, options)
    if(result instanceof Array) {
      return result.map(this.wrapMedia).join("\n")
    }
    return result ? this.wrapMedia(result) : result
  },
  wrapMedia: function(item) {
    if(item.media) {
      var css = "@media " + item.media + " {\n"
      css += item.css.join("\n")
      css += "} /* mq " + item.media + "*/\n\n"
      return css
    }
    return item.css.join("\n")
  }
}

if(isBrowser) {
  window.autocss = autocss
} else {
  module.exports = autocss
}
